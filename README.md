# klaxoon-testcafe

TestCafé testing with gherkin syntax

## Getting Started

Ensure that Node.js (version 6 or newer) and npm are installed on your computer 

## Usage

Clone gitlab repo
```bash
git clone git@gitlab.com:wixmove/klaxoon-testcafe.git
cd klaxoon-testcafe
```
Install required dependencies with npm
```bash
npm install
```
Done

## Script to launch tests

Run test in chrome browser
```bash
npm run test-chrome
```
Run test in firefox browser
```bash
npm run test-firefox
```
un test in chrome headless
```bash
npm run test-headless-chrome
```
Run test in firefox headless
```bash
npm run test-headless-firefox
```
