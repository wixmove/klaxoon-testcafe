import { When } from 'cucumber';
import { LoginPage } from '../page_model';

When(/I fill username with "(.+)" and submit form/, async (t, [username]) => {
    const randomId = Math.floor(Math.random() * 100000000);
    await t.typeText(LoginPage.usernameInput, username + randomId)
    await t.click(LoginPage.submitButton);
});