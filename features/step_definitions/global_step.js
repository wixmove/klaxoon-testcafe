import { Given } from 'cucumber';

Given(/I open meeting klaxoon (.+)/, async (t, [id]) => {
    await t.navigateTo(`https://test.klaxoon.io/join/${id}`);
});
