import { When, Then } from 'cucumber';
import { StormPage } from '../page_model';

When(/I select "(.+)" in storm list/, async (t, [name]) => {
    await t.click(StormPage.getStormListWithName(name), { speed: 0.5 });
});

When(/I enter ideas words in form field/, async (t, [], table) => {
    for (const word of table.raw()) {
        await t.typeText(StormPage.ideaInput, word[0]).click(StormPage.submitInput);
    }
});

When(/I open idea list/, async (t) => {
    await t.click(StormPage.ideaListInput);
});


Then(/I verify my ideas in list/, async (t, [], table) => {
    for (const value of table.raw()) {
        await t.expect(StormPage.getIdeaInlistWithName(value[0])).ok(`this idea ${value[0]} is not in list`);
    }
});