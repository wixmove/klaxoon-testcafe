import { Selector } from 'testcafe';

class Login {
    constructor() {
        this.usernameInput = Selector('#nickname');
        this.submitButton = Selector('button').withAttribute('type', 'submit');
    }
}

export default new Login();