import { Selector, t } from 'testcafe';

class Storm {
    constructor() {
        this.ideaInput = Selector('input').withAttribute('placeholder', 'Idée');
        this.submitInput = Selector('button').withAttribute('type', 'submit').withText('envoyer');
        this.ideaListInput = Selector('div').withAttribute('data-action', 'preview');
        this.listIdea = Selector('ul').child('.likable-list-item').child('p')
    }

    getStormListWithName(name) {
        return Selector('div').withAttribute('role', 'list').withText(name);
    }

    getIdeaInlistWithName(name) {
        return Selector('ul').child('.likable-list-item').child('p').withText(name).exists;
    }


}

export default new Storm();