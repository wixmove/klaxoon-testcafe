Feature: Storm test

Scenario: Add storm words in list
    Given I open meeting klaxoon ZYAEFZ
    When I fill username with "User" and submit form
    And I select "Un mot pour l'année écoulée?" in storm list
    When I enter ideas words in form field
    | Étoiles |
    | Année |
    | Surprise |
    And I open idea list
    Then I verify my ideas in list
    | Étoiles |
    | Année |
    | Surprise |